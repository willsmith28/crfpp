FROM python:3.10-slim-bullseye

WORKDIR /crfpp

RUN apt-get update && \
    apt-get install --no-install-recommends -yqq \
    build-essential

COPY . /crfpp/

RUN ./configure && \
    make && \
    make install && \
    ldconfig
